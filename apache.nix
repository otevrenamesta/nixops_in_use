{
  network.description = "Cvičení - web server";

  target_server =
    { config, pkgs, ... }:
    { services.httpd.enable = true;
      services.httpd.adminAddr = "webmaster@otevrenamesta.cz";
      services.httpd.documentRoot = "${pkgs.valgrind.doc}/share/doc/valgrind/html";
      networking.firewall.allowedTCPPorts = [ 80 ];
    };
}
