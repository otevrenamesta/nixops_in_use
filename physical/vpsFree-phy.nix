{
  target_server =
    { config, lib, pkgs, ... }:
    { 
      imports = [
        /root/build-vpsfree-templates/files/configuration.nix
      ];

      deployment.targetHost = "37.205.14.17";
      #deployment.targetHost = "83.167.228.98";

      #netboot.host = "proxy.otevrenamesta.cz";
      #netboot.acmeSSL = true;

      #web.acmeSSL = true;
    };
} 
