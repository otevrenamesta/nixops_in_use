{
  target_server =
    { config, pkgs, ... }:
    { deployment.targetHost = "<IP_cílového_stroje>";
      boot.loader.grub.device = "/dev/sda";
      fileSystems."/" =
        { device = "/dev/disk/by-uuid/<UUID_vašeho_disku>";
          fsType = "ext4";
        };
    };
}
