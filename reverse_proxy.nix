{
  network.description = "Nginx reverse proxy + Let's Encrypt";

  target_server =
    { config, pkgs, lib, ... }:

    {

      networking = {
        hostName =  lib.mkForce "proxy";
        firewall.allowedTCPPorts = [ 80 443 ];
      };

      services.openssh.enable = true;

      services.nginx = {
        enable = true;

        virtualHosts = {

         "_" = {
            default = true;
            locations = {
              "/" = {
                proxyPass = "http://127.0.0.1";
              };
            };
          };

         "my.wikipedia.otevrenamesta.cz" = {
            forceSSL = true;
            enableACME = true;

            locations = {
              "/" = {
                proxyPass = "https://en.wikipedia.org";
              };
            };
          };

          "my.linkeddata.otevrenamesta.cz" = {
            forceSSL = true;
            enableACME = true;

            locations = {
              "/" = {
                proxyPass = "http://linkeddata.org/";
              };
            };
          };
        };
      };
    };
}
